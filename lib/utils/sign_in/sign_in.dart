import 'package:google_sign_in/google_sign_in.dart';
import 'package:inme_mobile/utils/api/api.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';

class GoogleSignInApi {
  final GoogleSignIn _googleSignIn;

  GoogleSignInApi({GoogleSignIn? googleSignIn})
      : _googleSignIn = googleSignIn ?? GoogleSignIn();

  GoogleSignInAccount? getCurrentUser() => _googleSignIn.currentUser;

  Future<Map<String, String>> get authHeaders async {
    final String? idToken = (await getCurrentUser()?.authentication)?.idToken;
    return <String, String>{'Authorization': 'Bearer $idToken'};
  }

  Future<void> login({bool skipLogin = false}) async {
    var user = await _googleSignIn.signInSilently();
    user ??= await _googleSignIn.signIn();

    if (user != null && !skipLogin) {
      // await loginToAPI();
    }
  }

  Future<ResponseStatus> tryAutoLogin() async {
    var user = await _googleSignIn.signInSilently();
    if (user != null) {
      await loginToAPI();
      return ResponseStatus.success;
    }
    return ResponseStatus.error;
  }

  Future<void> logOut() => _googleSignIn.disconnect();

  Future<GoogleSignInAccount?> refresh() async {
    var user = getCurrentUser();
    if (user != null) {
      user = await _googleSignIn.signInSilently(reAuthenticate: true);
    }
    return user;
  }
}

var googleSignInApiInstance = GoogleSignInApi();