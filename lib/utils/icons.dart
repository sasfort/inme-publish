class InMeIcons {
  static const iconsDir = "assets/icons";
  static const inMeLogo = "$iconsDir/logo_inme.svg";
  static const onBoarding2 = "$iconsDir/on_boarding_2.svg";
  static const onBoarding3 = "$iconsDir/on_boarding_3.svg";
  static const computerFilter = "$iconsDir/computer_filter.svg";
  static const foodFilter = "$iconsDir/food_filter.svg";
  static const gameFilter = "$iconsDir/game_filter.svg";
  static const petsFilter = "$iconsDir/pets_filter.svg";
  static const createSuccess = "$iconsDir/create_success.svg";
  static const paymentConfirmation = "$iconsDir/payment_confirmation.svg";
  static const paymentConfirmationLoading =
      "$iconsDir/payment_confirmation_loading.svg";
  static const checklist = "$iconsDir/checklist.svg";
  static const unchecklist = "$iconsDir/declined.svg";
}
