import 'dart:developer';

import 'package:inme_mobile/utils/api/helper.dart';
import 'package:inme_mobile/utils/sign_in/sign_in.dart';

Future<Map<String, dynamic>> loginToAPI() async {
  var idToken =
      (await googleSignInApiInstance.getCurrentUser()?.authentication)?.idToken;
  var accessToken =
      (await googleSignInApiInstance.getCurrentUser()?.authentication)
          ?.accessToken;

  var res = await apiHelperInstance.post(
      '/oauth/resource', {'id_token': idToken, 'access_token': accessToken});
  if (res["status"] == 400) {
    // retry
    var user = await googleSignInApiInstance.refresh();
    idToken = (await user?.authentication)?.idToken;
    accessToken = (await user?.authentication)?.accessToken;
    res = await apiHelperInstance.post(
        '/oauth/resource', {'id_token': idToken, 'access_token': accessToken});
    log("retry success");
  }

  return res;
}
