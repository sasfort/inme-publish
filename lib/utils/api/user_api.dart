import 'package:inme_mobile/utils/api/api.dart';

class UserApi {
  static Future<Map<String, dynamic>> getUser() async {
    final response =
        await apiHelperInstance.getWithAuthHeader("/accounts/resource");

    if (response["status"] == 200) {
      return response["data"];
    } else {
      return {"status": "error"};
    }
  }

  static Future<Map<String, dynamic>> updateUser(
      Map<String, dynamic> data) async {
    final response =
        await apiHelperInstance.postWithAuthHeader("/accounts/resource/edit", data);
    if (response["status"] == 200) {
      return response["data"]["profile"];
    } else {
      return {"status": "error"};
    }
  }
}
