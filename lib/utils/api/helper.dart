import 'dart:convert';
import 'dart:developer';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:inme_mobile/utils/sign_in/sign_in.dart';

class ApiHelper {
  final String _baseUrl;
  final http.Client _client;

  ApiHelper({http.Client? client, String? baseUrl})
      : _client = client ?? http.Client(),
        _baseUrl = baseUrl ?? "http://localhost:8080/api/v1";

  Future<Map<String, dynamic>> post(String path,
      [Map<String, dynamic>? body, Map<String, String>? headers]) async {
    final response = await _client.post(Uri.parse('$_baseUrl$path'),
        headers: {'Content-Type': 'application/json', ...?headers},
        body: json.encoder.convert(body));
    log(response.body);
    final decodedBody = json.decode(response.body) as Map<String, dynamic>;
    return {"status": response.statusCode, "data": decodedBody};
  }

  Future<Map<String, dynamic>> put(String path,
      [Map<String, dynamic>? body, Map<String, String>? headers]) async {
    final response = await _client.put(Uri.parse('$_baseUrl$path'),
        headers: {'Content-Type': 'application/json', ...?headers},
        body: json.encoder.convert(body));
    log(response.body);
    final decodedBody = json.decode(response.body) as Map<String, dynamic>;
    return {"status": response.statusCode, "data": decodedBody};
  }

  Future<Map<String, dynamic>> get(String path,
      [Map<String, String>? headers]) async {
    final response = await _client.get(
      Uri.parse('$_baseUrl$path'),
      headers: headers,
    );
    log(response.body);
    final decodedBody = json.decode(response.body);
    return {"status": response.statusCode, "data": decodedBody};
  }

  Future<Map<String, dynamic>> delete(String path,
      [Map<String, dynamic>? body, Map<String, String>? headers]) async {
    final response = await _client.delete(Uri.parse('$_baseUrl$path'),
        headers: {'Content-Type': 'application/json', ...?headers},
        body: json.encoder.convert(body));
    log(response.body);
    return {"status": response.statusCode};
  }

  Future<Map<String, dynamic>> getWithAuthHeader(String path) async {
    var idToken =
        (await googleSignInApiInstance.getCurrentUser()?.authentication)
            ?.idToken;

    var res = await get(path, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var user = await googleSignInApiInstance.refresh();
      idToken = (await user?.authentication)?.idToken;
      res = await get(path, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }

  Future<Map<String, dynamic>> postWithAuthHeader(String path,
      [Map<String, dynamic>? body]) async {
    var idToken =
        (await googleSignInApiInstance.getCurrentUser()?.authentication)
            ?.idToken;

    var res = await post(path, body, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var user = await googleSignInApiInstance.refresh();
      idToken = (await user?.authentication)?.idToken;
      res = await post(path, body, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }

  Future<Map<String, dynamic>> putWithAuthHeader(String path,
      [Map<String, dynamic>? body]) async {
    var idToken =
        (await googleSignInApiInstance.getCurrentUser()?.authentication)
            ?.idToken;

    var res = await put(path, body, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var user = await googleSignInApiInstance.refresh();
      idToken = (await user?.authentication)?.idToken;
      res = await put(path, body, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }

  Future<Map<String, dynamic>> deleteWithAuthHeader(String path,
      [Map<String, dynamic>? body]) async {
    var idToken =
        (await googleSignInApiInstance.getCurrentUser()?.authentication)
            ?.idToken;
    var res = await delete(path, body, {'Authorization': 'Bearer $idToken'});
    if (res["status"] == 401) {
      // retry
      var user = await googleSignInApiInstance.refresh();
      idToken = (await user?.authentication)?.idToken;
      res = await delete(path, body, {'Authorization': 'Bearer $idToken'});
    }

    return res;
  }
}

var apiHelperInstance =
    ApiHelper(baseUrl: dotenv.env['BASE_URL']);
