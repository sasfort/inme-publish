import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/api/api.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';

class BusinessApi {
  static Future<ResponseStatus> createBusiness(
      Map<String, dynamic> data) async {
    final response =
        await apiHelperInstance.postWithAuthHeader("/business/", data);

    if (response["status"] == 201) {
      return ResponseStatus.success;
    } else if (response["data"].containsKey("non_field_errors")) {
      return ResponseStatus.duplicate;
    } else {
      return ResponseStatus.error;
    }
  }

  static Future<List<Map<String, dynamic>>?> getListBusiness() async {
    try {
      final response = await apiHelperInstance.getWithAuthHeader("/business/");
      if (response["status"] == 200) {
        final decodedResponse = (response["data"] as List)
            .map((e) => e as Map<String, dynamic>)
            .toList();
        return decodedResponse;
      }
      return null;
    } catch (e) {
      return null;
    }
  }

  static Future<ResponseStatus> updateBusiness(int id,
      Map<String, dynamic> data) async {
    final response =
        await apiHelperInstance.putWithAuthHeader("/business/$id/", data);

    if (response["status"] == 200) {
      return ResponseStatus.success;
    } else {
      return ResponseStatus.error;
    }
  }

  static Future<List<Business>> getMyBusiness() async {
    Map<String, dynamic> response =
        await apiHelperInstance.getWithAuthHeader("/business/current/");

    await Future.delayed(const Duration(seconds: 2));

    List<dynamic> collection = response["data"];
    List<Business> _listBusiness =
        collection.map((json) => Business.fromJson(json)).toList();

    return _listBusiness;
  }

  static Future<ResponseStatus> deleteBusiness(int id) async {
    Map<String, dynamic> response =
        await apiHelperInstance.deleteWithAuthHeader("/business/$id/");

    if (response["status"] == 204) {
      return ResponseStatus.success;
    } else {
      return ResponseStatus.error;
    }
  }
}
