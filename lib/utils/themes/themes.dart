import 'package:flutter/material.dart';

import 'inme_colors.dart';

ThemeData appTheme = ThemeData(
  primaryColor: InMeColors.green,
  textTheme: ThemeData.light().textTheme.copyWith(
        headline1: const TextStyle(
          fontSize: 48,
          fontWeight: FontWeight.bold,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        headline2: const TextStyle(
          fontSize: 32,
          fontWeight: FontWeight.bold,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        headline3: const TextStyle(
          fontSize: 28,
          fontWeight: FontWeight.bold,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        headline4: const TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.w700,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        headline5: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w600,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        headline6: const TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w600,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        subtitle1: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        subtitle2: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        bodyText1: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w400,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        bodyText2: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w400,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        caption: const TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        button: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.bold,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
        overline: const TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w500,
          color: InMeColors.black,
          letterSpacing: 0,
        ),
      ),
);
