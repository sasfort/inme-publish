import 'package:flutter/material.dart';

class InMeColors {
  static const Color logoBlue = Color(0xFF1F4068);
  static const Color logoGreen = Color(0xFF67B8AA);
  static const Color red = Color(0xFFE54C4C);
  static const Color orange = Color(0xFFF3954F);
  static const Color yellow = Color(0xFFFFC900);
  static const Color green = Color(0xFF206A5D);
  static const Color blue = Color(0xFF004AAD);
  static const Color purple = Color(0xFF7965DE);
  static const Color moreWhite = Color(0xFFFCFCFC);
  static const Color white = Color(0xFFEBECF1);
  static const Color grey = Color(0xFFC4C4C4);
  static const Color black = Color(0xFF1B1C25);
  static const Color moreBlack = Color(0xFF030303);

}
