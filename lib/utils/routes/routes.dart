class Routes {
  static const home = '/home';
  static const profile = '/profile';
  static const onBoard = '/on_board';
  static const editProfile = 'edit_profile';
  static const settings = '/settings';
  static const signIn = '/sign_in';
  static const listBusiness = '/list_business';
  static const myBusiness = '/my_business';
  static const payment = '/payment';
  static const viewBusiness = 'view_business';
  static const createBusiness = 'create_business';
  static const helpCenter = '/help_center';
  static const investorGuideline = '/investor_guideline';
  static const viewOwnBusiness = '/view_own_business';
  static const investorList = '/investor_list';
  static const businessGuideline = 'business_guideline';
  static const faq = '/faq';
  static const paymentConfirmation = '/payment_confirmation';
  static const paymentInformation = '/payment_information';
  static const paymentSuccess = '/payment_success';
  static const paymentUnsuccess = '/payment_unsuccess';
  static const paymentLoading = '/payment_loading';
  static const messageDetail = '/message_detail';
}
