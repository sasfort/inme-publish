import 'package:flutter/material.dart';
import 'routes.dart';
import 'package:inme_mobile/view/screens/screens.dart';

Widget getScreenByName(String name) {
  {
    switch (name) {
      case Routes.profile:
        return const ProfilePage();
      case Routes.editProfile:
        return const EditProfile();
      case Routes.home:
        return const MainNavigationScreen();
      case Routes.onBoard:
        return const OnBoardScreen();
      case Routes.settings:
        return const Settings();
      case Routes.signIn:
        return const SignInScreen();
      case Routes.listBusiness:
        return const InvestorList();
      case Routes.myBusiness:
        return const MyBusinessView();
      case Routes.payment:
        return const PaymentScreen();
      case Routes.viewBusiness:
        return const ViewBusiness();
      case Routes.createBusiness:
        return const CreateBusinessScreen();
      case Routes.investorGuideline:
        return const InvestorGuideline();
      case Routes.viewOwnBusiness:
        return const ViewOwnBusiness();
      case Routes.investorList:
        return const InvestorList();
      case Routes.businessGuideline:
        return const BusinessGuideline();
      case Routes.faq:
        return const Faq();
      case Routes.paymentConfirmation:
        return const PaymentConfirmation();
      case Routes.paymentInformation:
        return const PaymentInformation();
      case Routes.paymentSuccess:
        return const PaymentSuccess();
      case Routes.paymentUnsuccess:
        return const PaymentUnsuccess();
      case Routes.paymentLoading:
        return const PaymentLoading();
      case Routes.messageDetail:
        return const MessageDetail();
      case Routes.helpCenter:
        return const HelpCenter();
      default:
        return const OnBoardScreen();
    }
  }
}
