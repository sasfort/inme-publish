import 'package:flutter/foundation.dart';
import 'package:inme_mobile/models/user.dart';
import 'package:inme_mobile/utils/api/user_api.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';
import 'package:inme_mobile/utils/sign_in/sign_in.dart';

class UserProvider with ChangeNotifier {
  User? _user;

  User get user => _user!;

  Future<ResponseStatus> autoLogin() async {
    var response = await googleSignInApiInstance.tryAutoLogin();
    if (response == ResponseStatus.success) {
      await setUser();
      return ResponseStatus.success;
    } else {
      return ResponseStatus.error;
    }
  }

  Future<void> setUser() async {
    final user = await UserApi.getUser();
    _user = User.fromJson(user);
    notifyListeners();
  }

  void clearUser() {
    _user = null;
  }

  Future<bool> updateUser(Map<String, dynamic> data) async {
    Map<String, dynamic> decodedData;
    try {
      final user = await UserApi.updateUser(data);
      decodedData = {
        "name": user["nama"],
        "email": user["user"]["email"],
        "no_telp": user["no_telp"],
        "no_npwp": user["no_npwp"],
        "no_ktp": user["no_ktp"]
      };
      _user = User.fromJson(decodedData);
      notifyListeners();
      return true;
    } catch (e) {
      return false;
    }
  }

  bool isNPWPNotNull() {
    if (user.npwp != null) {
      return true;
    }
    return false;
  }
}
