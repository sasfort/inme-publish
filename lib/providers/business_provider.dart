import 'package:flutter/cupertino.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/api/business_api.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';

class BusinessProvider with ChangeNotifier {
  List<Business> _listBusiness = [];

  List<Business> get listBusiness => _listBusiness;

  Future<void> refreshListBusiness() async {
    final rawListBusiness = await BusinessApi.getListBusiness();
    List<Business> newListBusiness = [];
    if (rawListBusiness != null) {
      for (Map<String, dynamic> business in rawListBusiness) {
        newListBusiness.add(Business.fromJson(business));
      }
      _listBusiness = newListBusiness;
    } else {
      //Handle error if data is null
    }
  }

  Future<ResponseStatus> createBusiness(Map<String, dynamic> data) async {
    final response = await BusinessApi.createBusiness(data);
    if (response == ResponseStatus.success) {
      await refreshListBusiness();
      return response;
    } else if (response == ResponseStatus.duplicate) {
      return response;
    } else {
      return response;
    }
  }

  Future<ResponseStatus> deleteBusiness(int data) async {
    final response = await BusinessApi.deleteBusiness(data);
    if (response == ResponseStatus.success) {
       await refreshListBusiness();
       return response;
    } else {
      return response;
    }
  }

  Future<ResponseStatus> updateBusiness(int id, Map<String, dynamic> data) async {
    final response = await BusinessApi.updateBusiness(id, data);
    if (response == ResponseStatus.success) {
      await refreshListBusiness();
      return response;
    } else {
      return response;
    }
  }
}
