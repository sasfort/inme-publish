import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:inme_mobile/providers/_providers.dart';
import 'package:inme_mobile/providers/business_provider.dart';
import 'package:inme_mobile/utils/routes/routes_factory.dart';
import 'package:inme_mobile/utils/themes/themes.dart';
import 'package:provider/provider.dart';

void main() async {
  await dotenv.load(fileName: ".env");

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(
          create: (context) => UserProvider(),
        ),
        ChangeNotifierProvider<BusinessProvider>(
          create: (context) => BusinessProvider(),
        ),
      ],
      child: MaterialApp(
          title: 'InMe',
          theme: appTheme,
          onGenerateRoute: (settings) {
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => getScreenByName(settings.name!),
            );
          }),
    );
  }
}
