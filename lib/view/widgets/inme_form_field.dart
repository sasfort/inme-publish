import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class InMeFormField extends StatelessWidget {
  const InMeFormField({
    Key? key,
    required this.labelText,
    this.controller,
    this.type,
    this.isExpanded = false,
    this.formatter,
    this.validatorFunction,
    this.placeHolder,
    this.enabled,
    this.initialValue,
  }) : super(key: key);

  final String labelText;
  final String? Function(String?)? validatorFunction;
  final bool? enabled;
  final TextInputType? type;
  final String? placeHolder;
  final List<TextInputFormatter>? formatter;
  final TextEditingController? controller;
  final bool isExpanded;
  final String? initialValue;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          labelText,
          textAlign: TextAlign.start,
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 8.0),
        TextFormField(
          initialValue: initialValue,
          maxLines: isExpanded ? null : 1,
          enabled: enabled,
          controller: controller,
          keyboardType: type,
          inputFormatters: formatter,
          validator: validatorFunction,
          style: Theme.of(context).textTheme.bodyText2,
          decoration: InputDecoration(
            isDense: true,
            contentPadding: const EdgeInsets.all(10),
            fillColor: InMeColors.white,
            filled: true,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide.none,
            ),
            hintText: placeHolder,
            hintStyle: Theme.of(context).textTheme.caption!.copyWith(
                  fontWeight: FontWeight.bold,
                  color: InMeColors.grey,
                ),
          ),
        ),
      ],
    );
  }
}
