import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';

class SuccessScreen extends StatelessWidget {
  final String text;
  const SuccessScreen({Key? key, this.text = Words.createSuccess}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: Sizes.screenWidth(context) * 0.4,
            ),
            ConstrainedBox(
              child: SvgPicture.asset(InMeIcons.createSuccess),
              constraints: BoxConstraints(
                maxWidth: Sizes.screenWidth(context) * 0.6,
                maxHeight: Sizes.screenWidth(context) * 0.6,
              ),
            ),
            SizedBox(
              height: Sizes.screenWidth(context) * 0.2,
            ),
            Text(
              text,
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(color: InMeColors.green),
            ),
            const SizedBox(height: 16.0),
            GestureDetector(
              onTap: () => Navigator.pushNamedAndRemoveUntil(
                  context, Routes.home, (route) => false),
              child: Text(
                Words.backToHomepage,
                style: Theme.of(context)
                    .textTheme
                    .button!
                    .copyWith(color: InMeColors.logoBlue),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
