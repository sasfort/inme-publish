import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/images.dart';
import 'package:inme_mobile/utils/routes/routes.dart';

class CompanyCard extends StatelessWidget {
  const CompanyCard({
    Key? key,
    required this.companyData,
  }) : super(key: key);

  final Business companyData;

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(15),
      elevation: 4,
      child: ListTile(
        key: const Key('company card'),
        onTap: () {
          Navigator.pushNamed(context, Routes.viewBusiness, arguments: companyData);
        },
        contentPadding: const EdgeInsets.all(16.0),
        leading: const Image(
          image: AssetImage(InMeImages.inMeLogo),
          fit: BoxFit.fill,
        ),
        title: Text(
          companyData.name,
          style: Theme.of(context).textTheme.headline6!.copyWith(
                fontWeight: FontWeight.w700,
              ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(
            companyData.description.length > 80
                ? companyData.description.substring(0, 80) + '...'
                : companyData.description,
            maxLines: 3,
          ),
        ),
      ),
    );
  }
}
