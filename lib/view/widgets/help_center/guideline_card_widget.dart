import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';

class GuidelineCard extends StatelessWidget {
  const GuidelineCard({
    Key? key,
    required this.guideline,
    required this.index,
  }) : super(key: key);

  final List<Map<String, String>> guideline;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4,
      child: ListTile(
        key: const Key('guideline card'),
        onTap: () {},
        contentPadding:
            const EdgeInsets.symmetric(vertical: 18.0, horizontal: 16.0),
        title: Text(
          guideline[index]['title']!,
          style: Theme.of(context).textTheme.headline6!.copyWith(
                fontWeight: FontWeight.w700,
              ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Text(
            guideline[index]['description']!,
            style: Theme.of(context).textTheme.caption?.copyWith(
                  color: InMeColors.black,
                ),
          ),
        ),
      ),
    );
  }
}
