import 'package:flutter/material.dart';
import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:intl/intl.dart';

class BoxPaymentInformation extends StatelessWidget {
  final PaymentInfo paymentDetail;
  const BoxPaymentInformation({
    Key? key,
    required this.paymentDetail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: Sizes.screenWidth(context) * 0.8,
        height: Sizes.screenHeight(context) * 0.33,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            border: Border.all(color: InMeColors.grey)),
        child: Padding(
          padding: const EdgeInsets.only(left: 12.0, right: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 15.0,
              ),
              Text(
                "Invest",
                style: Theme.of(context).textTheme.subtitle1?.copyWith(
                    color: InMeColors.black, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 15.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    paymentDetail.businessName!,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        ?.copyWith(color: InMeColors.black),
                  ),
                  Text(NumberFormat.simpleCurrency(name: 'Rp. ')
                      .format(paymentDetail.investAmount))
                ],
              ),
              const SizedBox(
                height: 15.0,
              ),
              const Divider(
                color: InMeColors.grey,
                thickness: 1.0,
              ),
              const SizedBox(
                height: 9.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    Words.subTotal,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        ?.copyWith(color: InMeColors.black),
                  ),
                  Text(NumberFormat.simpleCurrency(name: 'Rp. ')
                      .format(paymentDetail.investAmount))
                ],
              ),
              const SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    Words.platformFee,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        ?.copyWith(color: InMeColors.black),
                  ),
                  Text(
                    "Rp. 50.000,00",
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        ?.copyWith(color: InMeColors.black),
                  ),
                ],
              ),
              const SizedBox(
                height: 10.0,
              ),
              const Divider(
                color: InMeColors.grey,
                thickness: 1.0,
              ),
              const SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    Words.total,
                    style: Theme.of(context).textTheme.bodyText2?.copyWith(
                        color: InMeColors.black, fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 1.0),
                    child: Text(NumberFormat.simpleCurrency(name: 'Rp. ')
                        .format(paymentDetail.totalPayment)),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
