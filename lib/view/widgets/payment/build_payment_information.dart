import 'package:flutter/material.dart';
import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/inme_rounded_button.dart';
import 'package:inme_mobile/view/widgets/payment/box_payment_information.dart';
import 'package:inme_mobile/view/widgets/payment/build_image.dart';

class BuildPaymentInformation extends StatelessWidget {
  final bool success;
  final PaymentInfo paymentDetail;
  const BuildPaymentInformation(
      {Key? key, required this.success, required this.paymentDetail})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 30.0,
          ),
          success
              ? paymentSuccess(context, paymentDetail)
              : paymentUnsuccess(context, paymentDetail),
        ],
      ),
    );
  }
}

Widget paymentSuccess(BuildContext context, PaymentInfo paymentInfo) {
  return Column(
    children: [
      const SizedBox(
        height: 20.0,
      ),
      Padding(
        padding: const EdgeInsets.only(top: 40.0),
        child: Center(child: buildImage(context, InMeIcons.checklist)),
      ),
      const SizedBox(
        height: 20.0,
      ),
      Text(
        "Thank You for Investing!",
        style: Theme.of(context)
            .textTheme
            .headline5
            ?.copyWith(color: InMeColors.black, fontWeight: FontWeight.bold),
      ),
      const SizedBox(
        height: 8.0,
      ),
      Text(
        "Your order has been succeed",
        style: Theme.of(context)
            .textTheme
            .bodyText2
            ?.copyWith(color: InMeColors.black),
      ),
      const SizedBox(
        height: 60.0,
      ),
      BoxPaymentInformation(
        paymentDetail: paymentInfo,
      ),
      const SizedBox(
        height: 30.0,
      ),
      SizedBox(
        width: Sizes.screenWidth(context) * 0.27,
        child: InMeRoundedButton(
            backgroundColor: InMeColors.logoBlue,
            onPressed: () => Navigator.pushNamed(context, Routes.home),
            text: Words.done),
      ),
    ],
  );
}

Widget paymentUnsuccess(BuildContext context, PaymentInfo paymentInfo) {
  return Column(
    children: [
      const SizedBox(
        height: 20.0,
      ),
      Padding(
        padding: const EdgeInsets.only(top: 40.0),
        child: Center(child: buildImage(context, InMeIcons.unchecklist)),
      ),
      const SizedBox(
        height: 20.0,
      ),
      Text(
        "Sorry, We Can't Find \n     Your Payment",
        style: Theme.of(context)
            .textTheme
            .headline5
            ?.copyWith(color: InMeColors.black, fontWeight: FontWeight.bold),
      ),
      const SizedBox(
        height: 8.0,
      ),
      const SizedBox(
        height: 60.0,
      ),
      BoxPaymentInformation(paymentDetail: paymentInfo),
      const SizedBox(
        height: 30.0,
      ),
      SizedBox(
        width: Sizes.screenWidth(context) * 0.35,
        height: Sizes.screenWidth(context) * 0.1,
        child: InMeRoundedButton(
            backgroundColor: InMeColors.logoBlue,
            onPressed: () => Navigator.popUntil(context,
                (route) => route.settings.name == Routes.paymentInformation),
            text: Words.paymentConfirmationNewLine),
      ),
    ],
  );
}
