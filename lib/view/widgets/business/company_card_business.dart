import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/routes/routes.dart';

class CompanyCardBusiness extends StatelessWidget {
  const CompanyCardBusiness({
    Key? key,
    required this.companyData,
    required this.index,
    required this.checker,
  }) : super(key: key);

  final List<Map<String, String>> companyData;
  final int index;
  final bool checker;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
      child: Material(
        borderRadius: BorderRadius.circular(20),
        elevation: 4,
        child: ListTile(
          dense: true,
          key: const Key('company card'),
          onTap: () {
            Navigator.pushNamed(context, Routes.viewOwnBusiness);
          },
          contentPadding:
              const EdgeInsets.symmetric(vertical: 7.0, horizontal: 16.0),
          leading: ClipOval(
            child: Image(
              image: AssetImage(companyData[index]['logo']!),
              fit: BoxFit.fill,
              width: 50,
            ),
          ),
          title: Text(
            companyData[index]['name']!,
            style: Theme.of(context).textTheme.headline6!.copyWith(
                  fontWeight: FontWeight.w700,
                ),
          ),
          subtitle: checker != false
              ? paddingInvestor(index, companyData, context)
              : paddingBusiness(index, companyData, context),
        ),
      ),
    );
  }
}

Widget paddingInvestor(int index, List companyData, BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      children: [
        const Text("Rp."),
        Expanded(
          child: Text(
            companyData[index]['nominal']!.length > 80
                ? companyData[index]['nominal']!.substring(0, 80) + '...'
                : companyData[index]['nominal']!,
            maxLines: 3,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
      ],
    ),
  );
}

Widget paddingBusiness(int index, List companyData, BuildContext context) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0),
    child: Row(
      children: [
        Expanded(
          child: Text(
            companyData[index]['deskripsi']!.length > 80
                ? companyData[index]['deskripsi']!.substring(0, 80) + '...'
                : companyData[index]['deskripsi']!,
            maxLines: 3,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
      ],
    ),
  );
}
