import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/view/screens/business/update_business_screen.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import "package:intl/intl.dart";
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/sizes.dart';

class BusinessDetail extends StatelessWidget {
  const BusinessDetail({
    Key? key,
    required this.progressBarPercentage,
    required this.businessDetail,
    this.ownBusiness = false,
  }) : super(key: key);

  final double progressBarPercentage;
  final Business businessDetail;
  final bool ownBusiness;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: const BoxDecoration(
            color: InMeColors.grey,
            borderRadius: BorderRadius.all(
              Radius.circular(
                15,
              ),
            ),
          ),
          child: ImageSlideshow(
            width: double.infinity,
            height: Sizes.screenWidth(context) * 0.9 * 9 / 16,
            initialPage: 0,
            indicatorColor: InMeColors.logoGreen,
            indicatorBackgroundColor: InMeColors.grey,
            children: [
              Image.asset(
                'assets/images/fresh_food.png',
                fit: BoxFit.cover,
              ),
              Image.asset(
                'assets/images/logo_inme.png',
                fit: BoxFit.cover,
              ),
            ],
            isLoop: true,
          ),
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.02,
        ),
        LinearPercentIndicator(
          width: Sizes.screenWidth(context) * 0.9,
          animation: true,
          lineHeight: 20.0,
          animationDuration: 500,
          percent: progressBarPercentage,
          barRadius: const Radius.circular(
            10,
          ),
          progressColor: progressBarPercentage == 1.0
              ? InMeColors.logoGreen
              : progressBarPercentage > 0.66
                  ? InMeColors.green
                  : progressBarPercentage > 0.33
                      ? InMeColors.yellow
                      : InMeColors.red,
          backgroundColor: InMeColors.grey,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.005,
        ),
        Center(
          child: Text(
            '${(progressBarPercentage * 100).toStringAsFixed(1)}% completed',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.03,
        ),
        Row(
          children: [
            Text(
              businessDetail.name,
              style: Theme.of(context).textTheme.headline4!.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            const SizedBox(width: 8.0),
            if (ownBusiness)
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => UpdateBusinessScreen(
                          businessData: businessDetail,
                        ),
                      ));
                },
                child: const Icon(
                  Icons.edit,
                  color: InMeColors.grey,
                ),
              )
          ],
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.005,
        ),
        Text(
          businessDetail.description,
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.03,
        ),
        Text(
          'Category',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                fontWeight: FontWeight.bold,
              ),
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.005,
        ),
        Text(
          businessDetail.category,
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.03,
        ),
        Text(
          'NPWP',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                fontWeight: FontWeight.bold,
              ),
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.005,
        ),
        Text(
          '123456789012345',
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.03,
        ),
        Text(
          'Asked Funds',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                fontWeight: FontWeight.bold,
              ),
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.005,
        ),
        Text(
          'Rp ${NumberFormat("#,##0", "en_US").format(businessDetail.target)}',
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.03,
        ),
        Text(
          'Date Created',
          style: Theme.of(context).textTheme.bodyText2!.copyWith(
                fontWeight: FontWeight.bold,
              ),
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.005,
        ),
        Text(
          '09/02/2022',
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: Sizes.screenHeight(context) * 0.02,
        ),
      ],
    );
  }
}
