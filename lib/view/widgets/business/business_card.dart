import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/images.dart';
import '../../../utils/routes/routes.dart';

class BusinessCard extends StatelessWidget {
  const BusinessCard({
    Key? key,
    required this.business,
  }) : super(key: key);

  final Business business;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Material(
            borderRadius: BorderRadius.circular(20),
            elevation: 4,
            child: ListTile(
              title: Text(business.name),
              leading: const ClipOval(
                child: FadeInImage(
                    image: AssetImage(InMeImages.inMeLogo),
                    placeholder: AssetImage(InMeImages.inMeLogo),
                    fit: BoxFit.fill,
                    height: 50,
                    width: 50),
              ),
              subtitle: Text(
                business.description,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              onTap: () {
                Navigator.pushNamed(context, Routes.viewOwnBusiness, arguments: business);
              },
            )));
  }
}
