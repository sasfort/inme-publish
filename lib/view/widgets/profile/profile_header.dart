import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/images.dart';

import 'package:inme_mobile/utils/themes/inme_colors.dart';

class HeaderEditProfile extends SliverPersistentHeaderDelegate {
  final Function()? function;
  final double height;

  HeaderEditProfile({
    this.function,
    required this.height,
  });

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox(
      height: height * 0.2,
      child: Stack(
        children: <Widget>[
          Container(
            height: height * 0.13,
            decoration: const BoxDecoration(
                color: InMeColors.green,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                )),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: GestureDetector(
              onTap: function,
              child: Container(
                height: 125,
                decoration: BoxDecoration(
                  color: InMeColors.moreWhite,
                  boxShadow: [
                    BoxShadow(
                      spreadRadius: 2,
                      blurRadius: 10,
                      color: Colors.black.withOpacity(0.1),
                    )
                  ],
                  shape: BoxShape.circle,
                  image: const DecorationImage(
                    image: AssetImage(InMeImages.inMeLogo),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => height * 0.2;

  @override
  double get minExtent => height * 0.2;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
