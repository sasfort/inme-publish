import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:inme_mobile/providers/user_provider.dart';

import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/view/widgets/inme_flushbar.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/sign_in/sign_in.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/view/widgets/loadiang_animation.dart';
import 'package:provider/provider.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Scaffold(body: InMeLoadingAnimation())
        : Scaffold(
            body: SafeArea(
                child: SizedBox(
                    height: Sizes.screenHeight(context),
                    child: Column(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(),
                        ),
                        _buildImage(context, InMeIcons.inMeLogo),
                        Expanded(
                          flex: 1,
                          child: Container(),
                        ),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            maxWidth: Sizes.screenWidth(context) * 0.8,
                          ),
                          child: _buildHeadline(
                              context,
                              "Sign in to connect between investors and business entrepreneurs",
                              InMeColors.black),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(),
                        ),
                        ConstrainedBox(
                          constraints: BoxConstraints(
                            maxWidth: Sizes.screenWidth(context) * 0.7,
                          ),
                          child: TextButton(
                              onPressed: () => _handleSignIn(context),
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        InMeColors.green),
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: const BorderSide(
                                        color: InMeColors.green),
                                  ),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 8.0),
                                child: Text(
                                  'Continue with Google',
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle2!
                                      .copyWith(color: InMeColors.white),
                                ),
                              )),
                        ),
                        Expanded(
                          flex: 5,
                          child: Container(),
                        ),
                      ],
                    ))));
  }

  Future<void> _handleSignIn(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    try {
      await googleSignInApiInstance.login();
      if (googleSignInApiInstance.getCurrentUser() != null) {
        await Provider.of<UserProvider>(context, listen: false).setUser();
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.home, (route) => false);
        InMeFlushBar.showSuccess(context, 'Sign in successful.');
      } else {
        // login cancelled
        setState(() {
          _isLoading = false;
        });
        const snackBar = SnackBar(content: Text("Sign in cancelled."));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        log("login cancelled");
      }
    } catch (e) {
      // network error
      setState(() {
        _isLoading = false;
      });
      var banner =
          MaterialBanner(content: const Text("Network error."), actions: [
        TextButton(
            onPressed: () async {
              ScaffoldMessenger.of(context).hideCurrentMaterialBanner();
              await _handleSignIn(context);
            },
            child: const Text("RETRY")),
        TextButton(
            onPressed: () =>
                ScaffoldMessenger.of(context).hideCurrentMaterialBanner(),
            child: const Text("DISMISS"))
      ]);
      ScaffoldMessenger.of(context).showMaterialBanner(banner);
      log("network error", error: e);
    }
  }
}

Widget _buildImage(BuildContext context, String imagePath) => ConstrainedBox(
      child: SvgPicture.asset(imagePath),
      constraints: BoxConstraints(
        maxWidth: Sizes.screenWidth(context) * 0.6,
        maxHeight: Sizes.screenWidth(context) * 0.6,
      ),
    );

Text _buildHeadline(BuildContext context, String text, Color color) => Text(
      text,
      style: Theme.of(context).textTheme.headline6!.copyWith(color: color),
      textAlign: TextAlign.center,
    );
