import 'package:flutter/material.dart';
import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/inme_rounded_button.dart';
import 'package:inme_mobile/view/widgets/payment/build_image.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';
import 'package:intl/intl.dart';

class PaymentConfirmation extends StatefulWidget {
  const PaymentConfirmation({Key? key}) : super(key: key);

  @override
  _PaymentConfirmationState createState() => _PaymentConfirmationState();
}

class _PaymentConfirmationState extends State<PaymentConfirmation> {
  late DateTime deadlinePayment;
  late DateTime now;

  @override
  void initState() {
    super.initState();
    setState(() {
      deadlinePayment = DateTime.now().add(const Duration(hours: 1));
      now = DateTime.now();
    });
  }

  @override
  Widget build(BuildContext context) {
    final String deadlineDate = DateFormat('d/M/y').format(deadlinePayment);
    final String deadlineHour = DateFormat.Hm().format(deadlinePayment);

    String nowDate = DateFormat('d/M/y').format(now);
    String nowHour = DateFormat.Hm().format(now);

    final paymentDetail =
        ModalRoute.of(context)?.settings.arguments as PaymentInfo;

    String virtualAccount = "89375198257${paymentDetail.codePayment}";

    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: GreenAppBar(
        appBar: AppBar(),
        title: Words.paymentConfirmation,
        arrowBack: false,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(50.0, 50.0, 50.0, 10.0),
            child: buildPaymentConfirmation(
                context,
                InMeIcons.paymentConfirmationLoading,
                Words.waitingPayment,
                Words.waitingPaymentSub),
          ),
          Text(deadlineDate),
          Text(deadlineHour),
          const SizedBox(
            height: 25.0,
          ),
          Text(
            "Virtual Account Code :",
            style: Theme.of(context).textTheme.subtitle1?.copyWith(
                color: InMeColors.black, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 25,
          ),
          Text(
            virtualAccount,
            style: Theme.of(context)
                .textTheme
                .subtitle2
                ?.copyWith(color: InMeColors.black),
          ),
          const SizedBox(
            height: 50.0,
          ),
          SizedBox(
            width: Sizes.screenWidth(context) * 0.4,
            child: InMeRoundedButton(
                backgroundColor: InMeColors.logoBlue,
                onPressed: () {
                  if (nowDate.compareTo(deadlineDate) < 0 &&
                      nowHour.compareTo(deadlineHour) < 0) {
                    Navigator.popUntil(
                        context,
                        (route) =>
                            route.settings.name == Routes.paymentInformation);
                  } else {
                    Navigator.pushNamed(context, Routes.paymentSuccess,
                        arguments: paymentDetail);
                  }
                },
                text: Words.paidThebill),
          )
        ],
      ),
    );
  }
}

Widget buildPaymentConfirmation(
    BuildContext context, String image, String headline1, String headline2) {
  return Column(
    children: [
      const SizedBox(
        height: 20.0,
      ),
      Center(child: buildImage(context, image)),
      const SizedBox(
        height: 12.0,
      ),
      Text(
        headline1,
        style: Theme.of(context)
            .textTheme
            .subtitle1
            ?.copyWith(color: InMeColors.black, fontWeight: FontWeight.bold),
      ),
      const SizedBox(
        height: 7.0,
      ),
      Text(
        headline2,
        style: Theme.of(context)
            .textTheme
            .subtitle2
            ?.copyWith(color: InMeColors.black),
      ),
    ],
  );
}
