import 'package:flutter/material.dart';
import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/view/widgets/payment/build_payment_information.dart';

class PaymentSuccess extends StatelessWidget {
  const PaymentSuccess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final paymentDetail =
        ModalRoute.of(context)?.settings.arguments as PaymentInfo;
    return BuildPaymentInformation(
      success: true,
      paymentDetail: paymentDetail,
    );
  }
}
