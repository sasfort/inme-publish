import 'package:flutter/material.dart';
import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/inme_alert_dialog.dart';
import 'package:inme_mobile/view/widgets/inme_rounded_button.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';
import 'package:intl/intl.dart';

class PaymentInformation extends StatelessWidget {
  const PaymentInformation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final paymentDetail =
        ModalRoute.of(context)?.settings.arguments as PaymentInfo;
    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: GreenAppBar(
        appBar: AppBar(),
        title: Words.paymentTitle,
        arrowBack: false,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: Sizes.screenHeight(context) * 0.08,
            ),
            Text(
              Words.invest,
              style: Theme.of(context).textTheme.subtitle1?.copyWith(
                  color: InMeColors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: Sizes.screenHeight(context) * 0.02,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    width: Sizes.screenWidth(context) * 0.8,
                    height: Sizes.screenHeight(context) * 0.08,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.0),
                        border: Border.all(color: InMeColors.grey)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 12.0, right: 25.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(paymentDetail.businessName!),
                          Text(NumberFormat.simpleCurrency(name: 'Rp. ')
                              .format(paymentDetail.investAmount))
                        ],
                      ),
                    )),
              ],
            ),
            SizedBox(
              height: Sizes.screenHeight(context) * 0.03,
            ),
            Text(
              Words.paymentMethod,
              style: Theme.of(context).textTheme.subtitle1?.copyWith(
                  color: InMeColors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: Sizes.screenHeight(context) * 0.02,
            ),
            Container(
                width: Sizes.screenWidth(context) * 0.8,
                height: Sizes.screenHeight(context) * 0.08,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    border: Border.all(color: InMeColors.grey)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 5.0,
                      ),
                      Text(paymentDetail.paymentOption!),
                      const SizedBox(
                        height: 9.0,
                      ),
                      const Text("**** ****048")
                    ],
                  ),
                )),
            SizedBox(
              height: Sizes.screenHeight(context) * 0.03,
            ),
            Text(
              Words.priceDetail,
              style: Theme.of(context).textTheme.subtitle1?.copyWith(
                  color: InMeColors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: Sizes.screenHeight(context) * 0.02,
            ),
            Container(
                width: Sizes.screenWidth(context) * 0.8,
                height: Sizes.screenHeight(context) * 0.16,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    border: Border.all(color: InMeColors.grey)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0, right: 30.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(Words.subTotal),
                          Text(NumberFormat.simpleCurrency(name: 'Rp. ')
                              .format(paymentDetail.investAmount)),
                        ],
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text(Words.platformFee),
                          Text("Rp.50.000,00"),
                        ],
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      const Divider(
                        color: InMeColors.grey,
                        thickness: 1.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(Words.total),
                          Padding(
                            padding: const EdgeInsets.only(right: 1.0),
                            child: Text(
                                NumberFormat.simpleCurrency(name: 'Rp. ')
                                    .format(paymentDetail.totalPayment)),
                          ),
                        ],
                      ),
                    ],
                  ),
                )),
            const SizedBox(
              height: 30.0,
            ),
            Row(
              children: [
                Expanded(
                  child: InMeRoundedButton(
                    backgroundColor: InMeColors.red,
                    onPressed: () => Navigator.of(context).pop(),
                    text: Words.cancel,
                  ),
                ),
                const SizedBox(width: 20),
                Expanded(
                    child: InMeRoundedButton(
                  backgroundColor: InMeColors.logoBlue,
                  text: Words.pay,
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => InMeAlertDialog(
                        title: Words.alertPayment,
                        buttonText1: Words.no,
                        buttonColor1: InMeColors.red,
                        buttonFunction1: () => Navigator.of(context).pop(),
                        buttonText2: Words.yes,
                        buttonColor2: InMeColors.logoBlue,
                        buttonFunction2: () => Navigator.pushNamed(
                            context, Routes.paymentConfirmation,
                            arguments: paymentDetail),
                      ),
                    );
                  },
                ))
              ],
            )
          ],
        ),
      ),
    );
  }
}
