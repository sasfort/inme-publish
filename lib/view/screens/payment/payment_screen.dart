import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:inme_mobile/models/payment.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen({Key? key}) : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  final _investKey = GlobalKey<FormState>();
  final _optionKey = GlobalKey<FormState>();
  String? paymentOption;
  String? investAmount;
  double? investAmountDouble;
  String? codePayment;

  @override
  Widget build(BuildContext context) {
    final businessDetail =
        ModalRoute.of(context)?.settings.arguments as Business?;

    return Scaffold(
        appBar: GreenAppBar(
          appBar: AppBar(),
          title: Words.paymentTitle,
          arrowBack: false,
        ),
        body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: Sizes.screenHeight(context) * 0.05),
                        Text(
                          "Do you want to invest in ${businessDetail?.name}?",
                          style: Theme.of(context).textTheme.headline6!,
                        ),
                        SizedBox(height: Sizes.screenHeight(context) * 0.05),
                        Form(
                            key: _investKey,
                            child: TextFormField(
                                onChanged: (value) {
                                  investAmount = value;
                                },
                                key: const Key('invest'),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return Words.enterPayment;
                                  } else if (_convertFunds(investAmount!) <
                                      50000) {
                                    return Words.valuePaymentRestriction;
                                  }
                                  return null;
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly,
                                  CurrencyTextInputFormatter(
                                    locale: 'id',
                                    symbol: 'Rp',
                                    decimalDigits: 0,
                                  )
                                ],
                                decoration: const InputDecoration(
                                    labelText: Words.investAmount,
                                    hintText: Words.investAmount))),
                        SizedBox(height: Sizes.screenHeight(context) * 0.08),
                        Text(
                          Words.paymentMethod,
                          style: Theme.of(context).textTheme.headline6!,
                        ),
                        SizedBox(height: Sizes.screenHeight(context) * 0.004),
                        Form(
                          key: _optionKey,
                          child: DropdownSearch<String>(
                            key: const Key('dropdown'),
                            mode: Mode.MENU,
                            showClearButton: true,
                            items: const [
                              "Virtual Banking BCA",
                              "Virtual Banking Mandiri",
                              "Virtual Banking BNI",
                              "Virtual Banking BRI",
                              "OVO",
                              "Gopay",
                              "Shopeepay"
                            ],
                            onChanged: (payment) {
                              paymentOption = payment;
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return Words.enterPaymentOpt;
                              }
                              return null;
                            },
                            dropdownSearchDecoration: const InputDecoration(
                              labelText: Words.paymentOption,
                              hintText: Words.selectPayment,
                            ),
                          ),
                        ),
                        SizedBox(height: Sizes.screenHeight(context) * 0.05),
                        Row(
                          children: [
                            TextButton(
                              key: const Key('proceed'),
                              style: TextButton.styleFrom(
                                padding: const EdgeInsets.all(16.10),
                                primary: InMeColors.white,
                                backgroundColor: InMeColors.logoBlue,
                                textStyle:
                                    Theme.of(context).textTheme.headline6!,
                              ),
                              onPressed: () {
                                final isInvestAmountFilled =
                                    _investKey.currentState!.validate();
                                final isOptionFilled =
                                    _optionKey.currentState!.validate();
                                if (isInvestAmountFilled && isOptionFilled) {
                                  investAmountDouble =
                                      _convertFunds(investAmount!);

                                  if (paymentOption == "Virtual Banking BCA") {
                                    codePayment = "01";
                                  } else if (paymentOption ==
                                      "Virtual Banking Mandiri") {
                                    codePayment = "02";
                                  } else if (paymentOption ==
                                      "Virtual Banking BNI") {
                                    codePayment = "03";
                                  } else if (paymentOption ==
                                      "Virtual Banking BRI") {
                                    codePayment = "04";
                                  } else if (paymentOption == "OVO") {
                                    codePayment = "05";
                                  } else if (paymentOption == "Gopay") {
                                    codePayment = "06";
                                  } else {
                                    codePayment = "07";
                                  }

                                  final paymentInformation = PaymentInfo(
                                      businessName: businessDetail?.name,
                                      codePayment: codePayment,
                                      investAmount: investAmountDouble,
                                      paymentOption: paymentOption,
                                      totalPayment:
                                          investAmountDouble! + 50000);

                                  Navigator.pushNamed(
                                      context, Routes.paymentInformation,
                                      arguments: paymentInformation);
                                }
                              },
                              child: const Text(Words.proceed),
                            ),
                          ],
                        ),
                      ]),
                ))));
  }
}

double _convertFunds(String data) {
  String newString = data.replaceAll(RegExp(r'[^0-9]'), '');
  return double.parse(newString);
}
