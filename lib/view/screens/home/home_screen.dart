import 'package:flutter/material.dart';
import 'package:inme_mobile/providers/user_provider.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/utils/images.dart';
import 'package:inme_mobile/view/widgets/inme_alert_dialog.dart';
import 'package:provider/provider.dart';

final List<String> imgList = [
  'assets/icons/home_carousel1.svg',
  'assets/icons/on_boarding_2.svg',
  'assets/icons/on_boarding_3.svg',
];

final List<Widget> imageSliders = imgList
    .map((item) => Container(
          margin: const EdgeInsets.all(5.0),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(
              Radius.circular(
                5.0,
              ),
            ),
            child: Stack(
              children: <Widget>[
                SvgPicture.asset(
                  item,
                  fit: BoxFit.fitHeight,
                  width: 1200,
                ),
              ],
            ),
          ),
        ))
    .toList();

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: InMeColors.green,
        child: SafeArea(
          child: Container(
            color: InMeColors.moreWhite,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    ClipRRect(
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      ),
                      child: Container(
                        color: InMeColors.green,
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: Sizes.screenHeight(context) * 0.04,
                            right: Sizes.screenWidth(context) * 0.05,
                            left: Sizes.screenWidth(context) * 0.05,
                            bottom: Sizes.screenHeight(context) * 0.03,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                'Welcome,',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline2!
                                    .copyWith(
                                      color: InMeColors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                              SizedBox(
                                height: Sizes.screenHeight(context) * 0.01,
                              ),
                              Consumer<UserProvider>(
                                builder: (context, user, _) => Text(
                                  user.user.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                        color: InMeColors.white,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, Routes.profile);
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                          top: Sizes.screenHeight(context) * 0.1,
                          left: Sizes.screenWidth(context) * 0.75,
                        ),
                        padding: const EdgeInsets.all(15),
                        width: Sizes.screenHeight(context) * 0.1,
                        height: Sizes.screenHeight(context) * 0.1,
                        decoration: BoxDecoration(
                          color: InMeColors.moreWhite,
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              color: InMeColors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset: const Offset(
                                  0, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Image.asset(
                          InMeImages.inMeLogo,
                          key: const Key('profpic'),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: Sizes.screenWidth(context) * 0.05,
                    vertical: Sizes.screenHeight(context) * 0.01,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        Words.features,
                        style: Theme.of(context).textTheme.headline4!,
                      ),
                      SizedBox(
                        height: Sizes.screenHeight(context) * 0.02,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(10),
                          ),
                          color: InMeColors.moreWhite,
                          boxShadow: [
                            BoxShadow(
                              color: InMeColors.grey.withOpacity(0.4),
                              spreadRadius: 4,
                              blurRadius: 4,
                              offset: const Offset(
                                  0, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: Sizes.screenHeight(context) * 0.02,
                            horizontal: Sizes.screenWidth(context) * 0.02,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    if (Provider.of<UserProvider>(context,
                                            listen: false)
                                        .isNPWPNotNull()) {
                                      Navigator.pushNamed(
                                          context, Routes.createBusiness);
                                    } else {
                                      showDialog(
                                        context: context,
                                        builder: (context) => InMeAlertDialog(
                                          title: Words.pleaseFillNPWP,
                                          buttonColor1: InMeColors.logoBlue,
                                          buttonText1: Words.yes,
                                          buttonFunction1: () {
                                            Navigator.of(context).pop();
                                            Navigator.pushNamed(
                                                context, Routes.editProfile);
                                          },
                                        ),
                                      );
                                    }
                                  },
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.create_new_folder,
                                        color: InMeColors.red,
                                        size:
                                            Sizes.screenHeight(context) * 0.05,
                                      ),
                                      const Text(
                                        Words.createBusiness,
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: Sizes.screenWidth(context) * 0.02,
                              ),
                              Expanded(
                                child: GestureDetector(
                                  key: const Key('mybusiness'),
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, Routes.myBusiness);
                                  },
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.business_center,
                                        color: InMeColors.logoBlue,
                                        size:
                                            Sizes.screenHeight(context) * 0.05,
                                      ),
                                      const Text(
                                        Words.myBusiness,
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: Sizes.screenWidth(context) * 0.02,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.account_balance_wallet,
                                      color: InMeColors.orange,
                                      size: Sizes.screenHeight(context) * 0.05,
                                    ),
                                    const Text(
                                      Words.investedBusiness,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: Sizes.screenWidth(context) * 0.02,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.receipt_long,
                                      color: InMeColors.logoGreen,
                                      size: Sizes.screenHeight(context) * 0.05,
                                    ),
                                    const Text(
                                      Words.investationHistory,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Sizes.screenHeight(context) * 0.05,
                      ),
                      RichText(
                        text: TextSpan(
                          style: Theme.of(context).textTheme.headline4!,
                          children: <TextSpan>[
                            const TextSpan(text: Words.why),
                            TextSpan(
                              text: ' In',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4!
                                  .copyWith(
                                    color: InMeColors.logoGreen,
                                  ),
                            ),
                            TextSpan(
                              text: 'Me',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4!
                                  .copyWith(
                                    color: InMeColors.logoBlue,
                                  ),
                            ),
                            const TextSpan(
                              text: '?',
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: Sizes.screenHeight(context) * 0.3,
                        child: Column(children: [
                          Expanded(
                            child: CarouselSlider(
                              items: imageSliders,
                              carouselController: _controller,
                              options: CarouselOptions(
                                  autoPlay: true,
                                  enlargeCenterPage: true,
                                  aspectRatio: 2.0,
                                  onPageChanged: (index, reason) {
                                    setState(() {
                                      _current = index;
                                    });
                                  }),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: imgList.asMap().entries.map((entry) {
                              return GestureDetector(
                                onTap: () =>
                                    _controller.animateToPage(entry.key),
                                child: Container(
                                  width: 8,
                                  height: 8,
                                  margin: const EdgeInsets.symmetric(
                                      vertical: 4, horizontal: 8),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: _current == entry.key
                                        ? InMeColors.orange
                                        : InMeColors.grey,
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
