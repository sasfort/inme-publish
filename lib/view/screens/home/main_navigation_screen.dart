import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/view/screens/screens.dart';

class MainNavigationScreen extends StatefulWidget {
  const MainNavigationScreen({Key? key}) : super(key: key);

  @override
  State<MainNavigationScreen> createState() => _MainNavigationScreenState();
}

class _MainNavigationScreenState extends State<MainNavigationScreen> {
  int _current = 0;
  static const List<Widget> _pages = <Widget>[
    HomeScreen(),
    SearchScreen(),
    MessageList(),
    Settings(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _current = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages.elementAt(_current),
      bottomNavigationBar: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
        child: SizedBox(
          height: Sizes.screenHeight(context) * 0.1,
          child: BottomNavigationBar(
            onTap: _onItemTapped,
            currentIndex: _current,
            iconSize: Sizes.screenHeight(context) * 0.04,
            selectedItemColor: InMeColors.yellow,
            unselectedItemColor: InMeColors.white,
            backgroundColor: InMeColors.green,
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                label: Words.home,
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.search,
                ),
                label: Words.search,
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.chat,
                ),
                label: Words.chat,
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.settings,
                  key: Key(
                    'settings',
                  ),
                ),
                label: Words.settings,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
