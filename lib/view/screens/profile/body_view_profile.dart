import 'package:flutter/material.dart';
import 'package:inme_mobile/providers/user_provider.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/profile/profile_header.dart';
import 'package:provider/provider.dart';

class BodyViewProfile extends StatelessWidget {
  const BodyViewProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = Sizes.screenHeight(context);
    return CustomScrollView(
      slivers: [
        SliverPersistentHeader(
          pinned: true,
          delegate: HeaderEditProfile(height: height),
        ),
        SliverFillRemaining(
          hasScrollBody: false,
          child: Consumer<UserProvider>(
            builder: (context, user, _) => Column(
              children: <Widget>[
                const ListTile(title: Text(Words.account)),
                ListTile(
                  title: const Text(Words.name),
                  subtitle: Text(user.user.name),
                ),
                ListTile(
                  title: const Text(Words.phoneNumber),
                  subtitle: Text(
                    user.user.phoneNumber != null ? user.user.phoneNumber! : '-',
                  ),
                ),
                ListTile(
                  title: const Text(Words.nik),
                  subtitle: Text(
                    user.user.nik != null ? user.user.nik! : '-',
                  ),
                ),
                ListTile(
                  title: const Text(Words.npwp),
                  subtitle: Text(
                    user.user.npwp != null ? user.user.npwp! : '-',
                  ),
                ),
                ListTile(
                  title: const Text(Words.email),
                  subtitle: Text(
                    user.user.email,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
