import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'body_edit_profile.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Body();
  }
}

AppBar buildAppBar() {
  return AppBar(
    elevation: 0,
    title: const Text(Words.editProfile),
    centerTitle: true,
    backgroundColor: InMeColors.green,
  );
}
