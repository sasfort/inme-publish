import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inme_mobile/providers/user_provider.dart';
import 'package:inme_mobile/view/widgets/inme_flushbar.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/inme_alert_dialog.dart';
import 'package:inme_mobile/view/widgets/inme_form_field.dart';
import 'package:inme_mobile/view/widgets/inme_rounded_button.dart';
import 'package:inme_mobile/view/widgets/loadiang_animation.dart';
import 'package:provider/provider.dart';
import 'edit_profile_screen.dart';
import '../../widgets/profile/profile_header.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final _formKey = GlobalKey<FormState>();
  final regex = RegExp(r'^[\.a-zA-Z0-9,!? ]*$');
  late Map<String, TextEditingController> _formController;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _initializeUserData();
  }

  @override
  Widget build(BuildContext context) {
    final height = Sizes.screenHeight(context);
    return _isLoading
        ? const Scaffold(body: InMeLoadingAnimation())
        : Scaffold(
            backgroundColor: InMeColors.moreWhite,
            appBar: buildAppBar(),
            body: CustomScrollView(
              slivers: [
                SliverPersistentHeader(
                  pinned: true,
                  delegate: HeaderEditProfile(height: height),
                ),
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      children: <Widget>[
                        Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              InMeFormField(
                                controller: _formController["name"],
                                key: const Key(Words.name),
                                labelText: Words.name,
                                enabled: false,
                              ),
                              const SizedBox(height: 16.0),
                              InMeFormField(
                                controller: _formController["phone_number"],
                                key: const Key(Words.phoneNumber),
                                labelText: Words.phoneNumber,
                                placeHolder: Words.phonePlaceholder,
                                type: TextInputType.number,
                                enabled: true,
                                formatter: [FilteringTextInputFormatter.digitsOnly],
                                validatorFunction: (value) {
                                  if (value == null || value.isEmpty) {
                                    return Words.cannotEmpty;
                                  } else if (value.length < 10) {
                                    return Words.tooShortPhone;
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              InMeFormField(
                                controller: _formController["nik"],
                                key: const Key(Words.nik),
                                labelText: Words.nik,
                                placeHolder: Words.nikPlaceholder,
                                type: TextInputType.number,
                                enabled: true,
                                formatter: [FilteringTextInputFormatter.digitsOnly],
                                validatorFunction: (value) {
                                  if (value == null || value.isEmpty) {
                                    return Words.cannotEmpty;
                                  } else if (value.length != 16) {
                                    return Words.nik16Digit;
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              InMeFormField(
                                controller: _formController["npwp"],
                                key: const Key(Words.npwp),
                                labelText: Words.npwp,
                                placeHolder: Words.npwpPlaceholder,
                                type: TextInputType.number,
                                enabled: true,
                                formatter: [FilteringTextInputFormatter.digitsOnly],
                                validatorFunction: (value) {
                                  if (value == null || value.isEmpty) {
                                    return Words.cannotEmpty;
                                  } else if (value.length != 15) {
                                    return Words.npwp15Digit;
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              InMeFormField(
                                controller: _formController["email"],
                                labelText: Words.email,
                                enabled: false,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 16.0),
                        const Spacer(),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: InMeRoundedButton(
                                backgroundColor: InMeColors.red,
                                onPressed: () => Navigator.of(context).pop(),
                                text: Words.cancel,
                              ),
                            ),
                            const SizedBox(width: 20),
                            Expanded(
                              child: InMeRoundedButton(
                                backgroundColor: InMeColors.logoBlue,
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    final data = {
                                      "name": _formController["name"]!.text,
                                      "no_telp": _formController["phone_number"]!.text,
                                      "no_ktp": _formController["nik"]!.text,
                                      "no_npwp": _formController["npwp"]!.text
                                    };
                                    showDialog(
                                      context: context,
                                      builder: (context) => InMeAlertDialog(
                                        title: Words.alertTitle,
                                        buttonText1: Words.no,
                                        buttonColor1: InMeColors.red,
                                        buttonFunction1: () => Navigator.of(context).pop(),
                                        buttonText2: Words.yes,
                                        buttonColor2: InMeColors.logoBlue,
                                        buttonFunction2: () => _submitData(data),
                                      ),
                                    );
                                  }
                                },
                                text: Words.saveChanges,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  void _submitData(Map<String, dynamic> data) async {
    Navigator.of(context).pop();
    setState(() {
      _isLoading = true;
    });
    final response = await Provider.of<UserProvider>(context, listen: false).updateUser(data);
    if (response) {
      Navigator.of(context).pop();
      InMeFlushBar.showSuccess(context, Words.updateSucess);
    } else {
      setState(() {
        _isLoading = false;
      });
      InMeFlushBar.showFailed(context, Words.savingDataFailed);
    }
  }

  void _initializeUserData() {
    final user = Provider.of<UserProvider>(context, listen: false).user;
    _formController = {
      "name": TextEditingController(text: user.name),
      "phone_number": TextEditingController(text: user.phoneNumber),
      "nik": TextEditingController(text: user.nik),
      "npwp": TextEditingController(text: user.npwp),
      "email": TextEditingController(text: user.email)
    };
  }
}
