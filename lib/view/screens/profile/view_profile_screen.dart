import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'body_view_profile.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  static const String routeName = '/profile_page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: appBarProfile(context),
      body: const BodyViewProfile(),
    );
  }
}

AppBar appBarProfile(context) {
  return AppBar(
    leading: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pushNamed(context, Routes.home);
        }),
    elevation: 0,
    title: const Text(Words.profile),
    actions: <Widget>[
      IconButton(
          icon: const Icon(Icons.edit),
          key: const Key(
            'edit',
          ),
          onPressed: () {
            Navigator.pushNamed(context, Routes.editProfile);
          })
    ],
    centerTitle: true,
    backgroundColor: InMeColors.green,
  );
}
