import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/settings/long_button.dart';

class HelpCenter extends StatelessWidget {
  const HelpCenter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: AppBar(
        backgroundColor: InMeColors.green,
        automaticallyImplyLeading: false,
        title: Text(Words.helpCenter,
            style: Theme.of(context)
                .textTheme
                .headline4
                ?.copyWith(color: InMeColors.white, fontWeight: FontWeight.bold)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: Sizes.screenWidth(context) * 0.3,
            ),
            LongButton(
                key: const Key('faq'),
                label: Words.frequentlyAskedQuestions,
                onTap: () => Navigator.pushNamed(context, Routes.faq),
                color: InMeColors.logoBlue),
            SizedBox(
              height: Sizes.screenWidth(context) * 0.06,
            ),
            LongButton(
                key: const Key('investor'),
                label: Words.investorGuideline,
                onTap: () => Navigator.pushNamed(context, Routes.investorGuideline),
                color: InMeColors.logoGreen),
            SizedBox(
              height: Sizes.screenWidth(context) * 0.06,
            ),
            LongButton(
                key: const Key('business'),
                label: Words.businessGuideline,
                onTap: () => Navigator.pushNamed(context, Routes.businessGuideline),
                color: InMeColors.logoBlue)
          ],
        ),
      ),
    );
  }
}
