import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/help_center/guideline_card_widget.dart';

class Faq extends StatelessWidget {
  const Faq({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _frequentAskedQuestion = [
      {
        'title': '1. What is InMe?',
        'description':
        'InMe is an application which helps entrepreneur meet investor.',
      },
      {
        'title': '2. What are the fees?',
        'description':
        'InMe will collect payment processing fee 2%.',
      },
      {
        'title': '3. How to edit my profile?',
        'description':
        'You can change your profile in InMe app by choosing setting page, then clicking ‘Profile’. In the top right corner of Profile page, you can clicking pencil icon.',
      },
      {
        'title': '4. What if i can’t pay by the payment deadline?',
        'description':
        'You have to make the payment again from the beginning.',
      },
      {
        'title':
        '5. Where do investor come from?',
        'description':
        'This will be your friends, familys, fans, co-workers, and people you’re connected to through your extended network. If people in your network like your business, they will spread the word to their friends, and so on.',
      }
    ];

    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: AppBar(
        backgroundColor: InMeColors.green,
        automaticallyImplyLeading: false,
        title: Text(
          Words.frequentlyAskedQuestions,
          style: Theme.of(context)
              .textTheme
              .headline4
              ?.copyWith(color: InMeColors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView.builder(
          itemCount: _frequentAskedQuestion.length,
          itemBuilder: (context, index) {
            return Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child:
                GuidelineCard(guideline: _frequentAskedQuestion, index: index));
          }),
    );
  }
}
