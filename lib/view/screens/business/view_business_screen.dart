import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/view/widgets/business/business_detail.dart';

class ViewBusiness extends StatelessWidget {
  const ViewBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double progressBarPercentage = 0.6;
    final businessDetail =
        ModalRoute.of(context)!.settings.arguments as Business;
    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(
            horizontal: Sizes.screenWidth(context) * 0.05,
            vertical: Sizes.screenHeight(context) * 0.025,
          ),
          children: [
            BusinessDetail(
                progressBarPercentage: progressBarPercentage,
                businessDetail: businessDetail),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  height: Sizes.screenHeight(context) * 0.06,
                  width: Sizes.screenWidth(context) * 0.25,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: InMeColors.logoBlue,
                      primary: InMeColors.white,
                      textStyle:
                          Theme.of(context).textTheme.bodyText2!.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                    onPressed: () {},
                    child: const Text(
                      'Message',
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                SizedBox(
                  width: Sizes.screenWidth(context) * 0.02,
                ),
                SizedBox(
                  height: Sizes.screenHeight(context) * 0.06,
                  width: Sizes.screenWidth(context) * 0.25,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: InMeColors.green,
                      primary: InMeColors.white,
                      textStyle:
                          Theme.of(context).textTheme.bodyText2!.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.payment,
                          arguments: businessDetail);
                    },
                    child: const Text(
                      'Invest',
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
