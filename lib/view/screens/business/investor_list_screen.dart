import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/business/company_card_business.dart';

class InvestorList extends StatefulWidget {
  const InvestorList({Key? key}) : super(key: key);

  @override
  State<InvestorList> createState() => InvestorListState();
}

class InvestorListState extends State<InvestorList> {
  final _companyData = [
    {
      'name': 'Nanaget',
      'nominal': '1.000.000',
      'logo': 'assets/images/nanaget.png',
    },
    {
      'name': "Fresh Food",
      'nominal': '10.000.000',
      'logo': 'assets/images/fresh_food.png',
    },
    {
      'name': 'Shoescare',
      'nominal': '1.000.000.000',
      'logo': 'assets/images/shoescare.png',
    }
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: InMeColors.moreWhite,
      appBar: AppBar(
        backgroundColor: InMeColors.green,
        automaticallyImplyLeading: false,
        title: const Text(
          Words.investorList,
          style: TextStyle(
            fontSize: 24,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 15),
        child: ListView.builder(
            itemCount: _companyData.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: CompanyCardBusiness(
                  companyData: _companyData,
                  index: index,
                  checker: true,
                ),
              );
            }),
      ),
    );
  }
}
