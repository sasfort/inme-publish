import 'package:flutter/material.dart';
import 'package:inme_mobile/providers/business_provider.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/view/widgets/widgets.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:provider/provider.dart';
import '../../../models/business.dart';
import 'package:inme_mobile/view/widgets/business/business_detail.dart';

class ViewOwnBusiness extends StatefulWidget {
  const ViewOwnBusiness({Key? key}) : super(key: key);

  @override
  State<ViewOwnBusiness> createState() => _ViewOwnBusinessState();
}

class _ViewOwnBusinessState extends State<ViewOwnBusiness> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    double progressBarPercentage = 0.6;
    final businessDetail =
        ModalRoute.of(context)!.settings.arguments as Business?;
    return Scaffold(
      body: _isLoading
          ? const InMeLoadingAnimation()
          : SafeArea(
              child: ListView(
                padding: EdgeInsets.symmetric(
                  horizontal: Sizes.screenWidth(context) * 0.05,
                  vertical: Sizes.screenHeight(context) * 0.025,
                ),
                children: [
                  BusinessDetail(
                      progressBarPercentage: progressBarPercentage,
                      businessDetail: businessDetail!,
                      ownBusiness: true,
                      ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: Sizes.screenHeight(context) * 0.06,
                        width: Sizes.screenWidth(context) * 0.25,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: InMeColors.red,
                            primary: InMeColors.white,
                            textStyle:
                                Theme.of(context).textTheme.bodyText2!.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                          ),
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (context) => InMeAlertDialog(
                                title: Words.businessDeleteAlert,
                                buttonText1: Words.no,
                                buttonColor1: InMeColors.red,
                                buttonFunction1: () => Navigator.of(context).pop(),
                                buttonText2: Words.yes,
                                buttonColor2: InMeColors.logoBlue,
                                buttonFunction2: () => _deleteBusiness(businessDetail.id),
                              ),
                            );
                          },
                          child: const Text(
                            'Delete',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: Sizes.screenWidth(context) * 0.02,
                      ),
                      SizedBox(
                        height: Sizes.screenHeight(context) * 0.06,
                        width: Sizes.screenWidth(context) * 0.25,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: InMeColors.green,
                            primary: InMeColors.white,
                            textStyle:
                                Theme.of(context).textTheme.bodyText2!.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, Routes.investorList);
                          },
                          child: const Text(
                            'Check Investor',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
    );
  }

  void _deleteBusiness(int id) async {
    setState(() {
      _isLoading = true;
    });
    final responseStatus =
        await Provider.of<BusinessProvider>(context, listen: false).deleteBusiness(id);
    switch (responseStatus) {
      case ResponseStatus.success:
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (context) => const SuccessScreen(text: "Delete success"),
            ),
            (route) => route.settings.name == Routes.home ? true : false);
        break;
      default:
        break;
    }
  }
}
