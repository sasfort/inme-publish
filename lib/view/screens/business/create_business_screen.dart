import 'package:flutter/material.dart';
import 'package:inme_mobile/providers/business_provider.dart';
import 'package:inme_mobile/providers/user_provider.dart';
import 'package:inme_mobile/utils/constants/response_status.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
// import 'package:inme_mobile/view/widgets/business/imagefield_form.dart';
import 'package:inme_mobile/view/widgets/business/inme_dropdown_form.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:inme_mobile/view/widgets/widgets.dart';
import 'package:provider/provider.dart';

class CreateBusinessScreen extends StatefulWidget {
  const CreateBusinessScreen({Key? key}) : super(key: key);

  @override
  State<CreateBusinessScreen> createState() => _CreateBusinessScreenState();
}

class _CreateBusinessScreenState extends State<CreateBusinessScreen> {
  // XFile? _xFileImage;
  bool _isLoading = false;
  final _businessNameFormKey = GlobalKey<FormState>();
  String? categoryValue;
  final _formController = {
    "business_name": TextEditingController(),
    "description": TextEditingController(),
    "target": TextEditingController()
  };
  final List<String> _categoryList = [
    'Food',
    'Game',
    'Pets',
    'Electronics',
  ];

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? const Scaffold(body: InMeLoadingAnimation())
        : Scaffold(
            appBar: WhiteAppBar(
              appBar: AppBar(),
              title: Words.createBusiness,
            ),
            body: CustomScrollView(
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Form(
                      key: _businessNameFormKey,
                      child: _createForm(context),
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  Column _createForm(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InMeFormField(
          labelText: Words.businessName,
          type: TextInputType.text,
          validatorFunction: _isFormNull,
          controller: _formController["business_name"],
        ),
        const SizedBox(height: 16.0),
        Text(
          Words.category,
          textAlign: TextAlign.start,
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 8.0),
        DropdownForm(
          categoryList: _categoryList,
          validator: _isFormNull,
          onCategorySelected: (value) {
            categoryValue = value;
          },
        ),
        const SizedBox(height: 16.0),
        InMeFormField(
          labelText: Words.description,
          type: TextInputType.multiline,
          isExpanded: true,
          validatorFunction: _isFormNull,
          controller: _formController["description"],
        ),
        const SizedBox(height: 16.0),
        Consumer<UserProvider>(
          builder: (context, user, _) => InMeFormField(
            labelText: Words.npwp,
            enabled: false,
            initialValue: user.user.npwp,
          ),
        ),
        const SizedBox(height: 16.0),
        InMeFormField(
          labelText: 'Asked Funds',
          formatter: [
            CurrencyTextInputFormatter(
              locale: 'id',
              symbol: 'Rp',
              decimalDigits: 0,
            )
          ],
          controller: _formController["target"],
          type: TextInputType.number,
          validatorFunction: _isFormNull,
        ),
        const SizedBox(height: 16.0),
        // Text(
        //   'Images',
        //   textAlign: TextAlign.start,
        //   style: Theme.of(context)
        //       .textTheme
        //       .bodyText2!
        //       .copyWith(fontWeight: FontWeight.bold),
        // ),
        // const SizedBox(height: 8.0),
        // GestureDetector(
        //   onTap: () async {
        //     await _pickImage();
        //     setState(() {});
        //   },
        //   child: ImageField(xFileImage: _xFileImage),
        // ),
        const Spacer(),
        const SizedBox(height: 16.0),
        _buildButtons(context),
      ],
    );
  }

  Row _buildButtons(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: InMeRoundedButton(
            text: Words.cancel,
            backgroundColor: InMeColors.red,
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          child: InMeRoundedButton(
            text: Words.create,
            backgroundColor: InMeColors.logoBlue,
            onPressed: () {
              if (_businessNameFormKey.currentState!.validate()) {
                final data = {
                  "business_name": _formController["business_name"]!.text,
                  "description": _formController["description"]!.text,
                  "category": categoryValue,
                  "target": _convertFunds(_formController["target"]!.text)
                };
                _submit(data);
              }
            },
          ),
        ),
      ],
    );
  }

  int _convertFunds(String data) {
    String newString = data.replaceAll(RegExp(r'[^0-9]'), '');
    return int.parse(newString);
  }

  void _submit(Map<String, dynamic> data) async {
    setState(() {
      _isLoading = true;
    });

    final response = await Provider.of<BusinessProvider>(context, listen: false)
        .createBusiness(data);
    if (response == ResponseStatus.success) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => const SuccessScreen(
                  text: Words.createSuccess,
                )),
      );
    } else if (response == ResponseStatus.duplicate) {
      setState(() {
        _isLoading = false;
      });
      showDialog(
        context: context,
        builder: (context) => InMeAlertDialog(
            title: Words.dataAlreadyExist,
            buttonText1: Words.yes,
            buttonFunction1: () => Navigator.of(context).pop(),
            buttonColor1: InMeColors.logoBlue),
      );
    } else {
      setState(() {
        _isLoading = false;
      });
      InMeFlushBar.showFailed(context, Words.savingDataFailed);
    }
  }

  String? _isFormNull(String? value) {
    if (value == null || value.isEmpty) {
      return Words.cannotEmpty;
    }
    return null;
  }

  // Future<void> _pickImage() async {
  //   final ImagePicker _picker = ImagePicker();
  //   _xFileImage = await _picker.pickImage(source: ImageSource.gallery);
  // }
}
