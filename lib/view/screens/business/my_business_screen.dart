import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/utils/api/api.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/business/business_card.dart';
import 'package:inme_mobile/view/widgets/payment/green_appbar.dart';

class MyBusinessView extends StatelessWidget {
  const MyBusinessView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyBusiness(listBusiness: BusinessApi.getMyBusiness());
  }
}

class MyBusiness extends StatelessWidget {
  final Future<List<Business>>? listBusiness;
  const MyBusiness({Key? key, this.listBusiness}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: GreenAppBar(
          appBar: AppBar(),
          title: Words.myBusiness,
          arrowBack: false,
        ),
        body: FutureBuilder(
          future: listBusiness,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.done:
                var listBusiness = snapshot.data;
                if (listBusiness.length == 0) {
                  return const Center(child: Text(Words.noBusinessFound));
                }
                return ListView.builder(
                    itemCount: listBusiness.length,
                    itemBuilder: (BuildContext context, int index) {
                      Business business = listBusiness[index];

                      return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: BusinessCard(
                            business: business,
                          ));
                    });
              default:
                return const Center(child: CircularProgressIndicator(color: InMeColors.green,));
            }
          },
        ));
  }
}
