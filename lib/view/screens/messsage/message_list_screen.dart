import 'package:flutter/material.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/white_appbar.dart';
import 'package:inme_mobile/view/widgets/message/widgets.dart';

class MessageList extends StatelessWidget {
  const MessageList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        appBar: AppBar(),
        title: Words.message,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        children: [
          const MessageCard(
            account: 'Account1',
            key: Key('a'),
          ),
          SizedBox(
            height: Sizes.screenHeight(context) * 0.01,
          ),
          const MessageCard(
            account: 'Account2',
          ),
          SizedBox(
            height: Sizes.screenHeight(context) * 0.01,
          ),
          const MessageCard(
            account: 'Account3',
          ),
          SizedBox(
            height: Sizes.screenHeight(context) * 0.01,
          ),
          const MessageCard(
            account: 'Account4',
          ),
          SizedBox(
            height: Sizes.screenHeight(context) * 0.01,
          ),
          const MessageCard(
            account: 'Account5',
          ),
          SizedBox(
            height: Sizes.screenHeight(context) * 0.01,
          ),
          const MessageCard(
            account: 'Account6',
          ),
          SizedBox(
            height: Sizes.screenHeight(context) * 0.01,
          ),
          const MessageCard(
            account: 'Account7',
          ),
        ],
      ),
    );
  }
}
