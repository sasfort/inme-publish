import 'package:flutter/material.dart';
import 'package:inme_mobile/providers/user_provider.dart';
import 'package:inme_mobile/utils/routes/routes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/screens/screens.dart';
import 'package:inme_mobile/view/widgets/white_appbar.dart';
import 'package:inme_mobile/view/widgets/settings/long_button.dart';
import 'package:inme_mobile/utils/sign_in/sign_in.dart';
import 'package:provider/provider.dart';

class Settings extends StatelessWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: WhiteAppBar(
          appBar: AppBar(),
          title: Words.settings,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            SizedBox(height: Sizes.screenWidth(context) * 0.2),
            LongButton(
              key: const Key('help center'),
              label: Words.helpCenter,
              onTap: () => Navigator.pushNamed(context, Routes.helpCenter),
              color: InMeColors.logoBlue,
            ),
            SizedBox(height: Sizes.screenWidth(context) * 0.02),
            LongButton(
              key: const Key('profile'),
              label: Words.profile,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ProfilePage()),
                );
              },
              color: InMeColors.logoGreen,
            ),
            SizedBox(height: Sizes.screenWidth(context) * 0.02),
            LongButton(
              key: const Key('log out'),
              label: Words.logOut,
              onTap: () {
                googleSignInApiInstance.logOut();
                Provider.of<UserProvider>(context, listen: false).clearUser();
                Navigator.pushNamedAndRemoveUntil(
                    context, Routes.signIn, (route) => false);
              },
              color: InMeColors.red,
            ),
          ]),
        ));
  }
}
