import 'package:flutter/material.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/providers/business_provider.dart';
import 'package:inme_mobile/utils/icons.dart';
import 'package:inme_mobile/utils/sizes.dart';
import 'package:inme_mobile/utils/themes/inme_colors.dart';
import 'package:inme_mobile/utils/words.dart';
import 'package:inme_mobile/view/widgets/search_and_filter/widgets.dart';
import 'package:provider/provider.dart';

enum Filter {
  food,
  game,
  computer,
  pets,
}

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<Business> _companyData = [];
  List<Business>? _searchedCompanyData;

  Filter? _filter;

  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    refreshBusinessList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        appBar: AppBar(),
        title: Words.search,
      ),
      backgroundColor: InMeColors.moreWhite,
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: SizedBox(
              height: 50,
              width: Sizes.screenWidth(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    key: const Key('search bar'),
                    child: SearchBar(
                      onChanged: _onSearchChanged,
                    ),
                  ),
                  Padding(
                    key: const Key('filter icon'),
                    padding: const EdgeInsets.only(left: 15.0),
                    child: GestureDetector(
                      onTap: () {
                        _showFilterBottomSheet(context);
                      },
                      child: const Icon(
                        Icons.filter_list_rounded,
                        size: 24,
                        color: InMeColors.black,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 4.0),
          Expanded(
            child: _isLoading
                ? const Center(
                    child: CircularProgressIndicator(color: InMeColors.green))
                : RefreshIndicator(
                    displacement: 20,
                    color: InMeColors.green,
                    onRefresh: () async {
                      await refreshBusinessList();
                    },
                    child: _searchedCompanyData!.isNotEmpty
                        ? ListView.builder(
                            physics: const BouncingScrollPhysics(
                                parent: AlwaysScrollableScrollPhysics()),
                            itemCount: _searchedCompanyData!.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 8.0,
                                  horizontal: 16.0,
                                ),
                                child: CompanyCard(
                                  companyData: _searchedCompanyData![index],
                                ),
                              );
                            },
                          )
                        : ListView(
                            children: [
                              SizedBox(
                                  height: Sizes.screenHeight(context) * 0.3),
                              Text(
                                Words.noBusinessFound,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .copyWith(
                                      fontWeight: FontWeight.w700,
                                      color: InMeColors.grey,
                                    ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                  ),
          ),
        ],
      ),
    );
  }

  void _onSearchChanged(String value) {
    _searchedCompanyData = [];
    for (var company in _companyData) {
      if (company.name.toLowerCase().contains((value.toLowerCase()))) {
        setState(() {
          _searchedCompanyData!.add(company);
        });
      }
    }
  }

  void _onFilterChanged(String filter) {
    _searchedCompanyData = [];
    for (var company in _companyData) {
      if (filter != '') {
        if (company.category.toLowerCase() == filter.toLowerCase()) {
          _searchedCompanyData!.add(company);
        }
      } else {
        _searchedCompanyData!.add(company);
      }
    }
    setState(() {});
  }

  String _filterAssignment(Filter? value) {
    if (value == Filter.food) {
      return 'Food';
    } else if (value == Filter.computer) {
      return 'Electronics';
    } else if (value == Filter.game) {
      return 'Game';
    } else if (value == Filter.pets) {
      return 'Pets';
    } else {
      return '';
    }
  }

  Future<void> refreshBusinessList() async {
    await Provider.of<BusinessProvider>(context, listen: false)
        .refreshListBusiness();
    _companyData =
        Provider.of<BusinessProvider>(context, listen: false).listBusiness;
    _searchedCompanyData = _companyData;
    setState(() {
      _isLoading = false;
    });
  }

  Future<dynamic> _showFilterBottomSheet(BuildContext context) async {
    return showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
        ),
        builder: (context) {
          return Wrap(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Text(
                  Words.filterBy,
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              FilterContent(
                icon: InMeIcons.foodFilter,
                label: Words.food,
                choosed: _filter == Filter.food ? true : false,
                function: () {
                  setState(() {
                    if (_filter != Filter.food) {
                      _filter = Filter.food;
                    } else {
                      _filter = null;
                    }
                  });
                  _onFilterChanged(_filterAssignment(_filter));
                  Navigator.of(context).pop();
                },
              ),
              FilterContent(
                icon: InMeIcons.gameFilter,
                label: Words.game,
                choosed: _filter == Filter.game ? true : false,
                function: () {
                  setState(() {
                    if (_filter != Filter.game) {
                      _filter = Filter.game;
                    } else {
                      _filter = null;
                    }
                  });
                  _onFilterChanged(_filterAssignment(_filter));
                  Navigator.of(context).pop();
                },
              ),
              FilterContent(
                icon: InMeIcons.petsFilter,
                label: Words.pets,
                choosed: _filter == Filter.pets ? true : false,
                function: () {
                  setState(() {
                    if (_filter != Filter.pets) {
                      _filter = Filter.pets;
                    } else {
                      _filter = null;
                    }
                  });
                  _onFilterChanged(_filterAssignment(_filter));
                  Navigator.of(context).pop();
                },
              ),
              FilterContent(
                icon: InMeIcons.computerFilter,
                label: Words.electronics,
                choosed: _filter == Filter.computer ? true : false,
                function: () {
                  setState(() {
                    if (_filter != Filter.computer) {
                      _filter = Filter.computer;
                    } else {
                      _filter = null;
                    }
                  });
                  _onFilterChanged(_filterAssignment(_filter));
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}
