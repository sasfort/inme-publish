class PaymentInfo {
  String? businessName;
  double? investAmount;
  double platformFee = 50000;
  double? totalPayment;
  String? paymentOption;
  String? codePayment;

  PaymentInfo(
      {required this.businessName,
      required this.investAmount,
      required this.totalPayment,
      required this.paymentOption,
      required this.codePayment});
}
