class User {
  User({
    required this.name,
    required this.email,
    this.phoneNumber,
    this.nik,
    this.npwp,
  });

  String name;
  String email;
  String? phoneNumber;
  String? nik;
  String? npwp;

  factory User.fromJson(Map<String, dynamic> json) => User(
        name: json["name"],
        email: json["email"],
        phoneNumber: json["no_telp"],
        nik: json["no_ktp"],
        npwp: json["no_npwp"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "phoneNumber": phoneNumber,
        "nik": nik,
        "npwp": npwp,
      };
}
