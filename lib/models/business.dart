class Business {
  int id;
  String name;
  String description;
  String category;
  double target;
  double investedFunds;

  Business({
    required this.id,
    required this.name,
    required this.description,
    required this.target,
    required this.category,
    required this.investedFunds,
  });

  factory Business.fromJson(Map<String, dynamic> json) {
    return Business(
      id: json['id'],
      name: json['business_name'],
      description: json['description'],
      category: json['category'],
      target: double.parse(json["target"]),
      investedFunds: double.parse(json["invested_funds"]),
    );
  }
}
