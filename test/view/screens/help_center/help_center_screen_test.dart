import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/screens/screens.dart';
import 'package:inme_mobile/utils/routes/routes_factory.dart';

void main() {
  testWidgets('Test the Frequently Asked Questions is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: const HelpCenter(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (_) => getScreenByName(settings.name!),
          settings: settings,
        );
      },
    ));
    expect(find.text('Frequently Asked Questions'), findsOneWidget);
  });

  testWidgets('Test the Investor Guideline is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: const InvestorGuideline(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (_) => getScreenByName(settings.name!),
          settings: settings,
        );
      },
    ));
    expect(find.text('Investor Guideline'), findsOneWidget);
  });

  testWidgets('Test the Business Guideline is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: const BusinessGuideline(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (_) => getScreenByName(settings.name!),
          settings: settings,
        );
      },
    ));
    expect(find.text('Business Guideline'), findsOneWidget);
  });

  testWidgets('Test the FAQ is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: const Faq(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (_) => getScreenByName(settings.name!),
          settings: settings,
        );
      },
    ));
    expect(find.text('Frequently Asked Questions'), findsOneWidget);
  });
}
