import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/screens/screens.dart';

void main() {
  testWidgets('Test the investor list is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: InvestorList()));
    expect(find.text('Investor List'), findsOneWidget);
  });
}
