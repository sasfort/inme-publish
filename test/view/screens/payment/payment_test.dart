import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/utils/routes/routes_factory.dart';
import 'package:inme_mobile/view/screens/payment/payment_screen.dart';
import 'package:inme_mobile/view/screens/screens.dart';

void main() {
  testWidgets('Test the Payment Screen widget is pumped',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: PaymentScreen()));
    expect(find.text('Payment Information'), findsOneWidget);
    expect(find.text('Payment Method'), findsOneWidget);
  });

  // testWidgets('Test when all the field is tap/filled',
  //     (WidgetTester tester) async {
  //   final investField = find.byKey(const Key("invest"));
  //   final dropdownField = find.byKey(const Key("dropdown"));
  //   final proceedButton = find.byKey(const Key("proceed"));
  //
  //   await tester.pumpWidget(MaterialApp(
  //     home: const PaymentScreen(),
  //     onGenerateRoute: (settings) {
  //       return MaterialPageRoute(
  //         builder: (_) => getScreenByName(settings.name!),
  //         settings: settings,
  //       );
  //     },
  //   ));
  //
  //   await tester.tap(investField);
  //   await tester.enterText(investField, '40000');
  //
  //   await tester.tap(dropdownField);
  //   await tester.pumpAndSettle();
  //
  //   final dropdownItem = find.text('Virtual Banking BCA').last;
  //
  //   await tester.tap(dropdownItem);
  //   await tester.pumpAndSettle();
  //
  //   await tester.tap(proceedButton);
  //   await tester.pumpAndSettle();
  //
  //   expect(find.text('Payment Information'), findsOneWidget);
  // });

  testWidgets('Test when all the field tap/filled (negative)',
      (WidgetTester tester) async {
    final investField = find.byKey(const Key("invest"));
    final dropdownField = find.byKey(const Key("dropdown"));
    final proceedButton = find.byKey(const Key("proceed"));

    await tester.pumpWidget(MaterialApp(
      home: const PaymentScreen(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (_) => getScreenByName(settings.name!),
          settings: settings,
        );
      },
    ));

    await tester.tap(investField);
    await tester.enterText(investField, 'aaaa');

    await tester.tap(dropdownField);
    await tester.pumpAndSettle();

    final dropdownItem = find.text('Virtual Banking BCA').last;

    await tester.tap(dropdownItem);
    await tester.pumpAndSettle();

    await tester.tap(proceedButton);
    await tester.pumpAndSettle();

    expect(find.text('Please enter your invest amount'), findsOneWidget);
  });
}
