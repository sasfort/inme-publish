import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/view/widgets/payment/box_payment_information.dart';
import 'package:network_image_mock/network_image_mock.dart';

void main() {
  testWidgets('Test the box payment information is created', (WidgetTester tester) async {
    await mockNetworkImagesFor(
      () => tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: BoxPaymentInformation(
              paymentDetail: PaymentInfo(
                businessName: 'nama',
                codePayment: 'kode',
                investAmount: 1000,
                paymentOption: 'what',
                totalPayment: 1000,
              ),
            ),
          ),
        ),
      ),
    );
    expect(find.text("nama"), findsOneWidget);
  });
}
