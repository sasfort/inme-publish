import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/models/payment.dart';
import 'package:inme_mobile/view/widgets/payment/build_payment_information.dart';
import 'package:network_image_mock/network_image_mock.dart';

void main() {
  testWidgets('Test the build payment information is success', (WidgetTester tester) async {
    await mockNetworkImagesFor(
      () => tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: BuildPaymentInformation(
              paymentDetail: PaymentInfo(
                businessName: 'nama',
                codePayment: 'kode',
                investAmount: 1000,
                paymentOption: 'what',
                totalPayment: 1000,
              ),
              success: true,
            ),
          ),
        ),
      ),
    );
    expect(find.text("Thank You for Investing!"), findsOneWidget);
  });

  testWidgets('Test the build payment information is failed', (WidgetTester tester) async {
    await mockNetworkImagesFor(
      () => tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: BuildPaymentInformation(
              paymentDetail: PaymentInfo(
                businessName: 'nama',
                codePayment: 'kode',
                investAmount: 1000,
                paymentOption: 'what',
                totalPayment: 1000,
              ),
              success: false,
            ),
          ),
        ),
      ),
    );
    expect(find.text("Sorry, We Can't Find \n     Your Payment"), findsOneWidget);
  });
}
