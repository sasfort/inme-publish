import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/utils/routes/routes_factory.dart';
import 'package:inme_mobile/view/screens/screens.dart';

void main() {
  testWidgets('Test the message card is long pressed', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: MessageList()));
    expect(find.text("Account1"), findsOneWidget);

    final card = find.byKey(const Key('a'));
    await tester.longPress(card); // Animate to the last page of on board
    await tester.pumpAndSettle();

    expect(find.text('Delete this chat?'), findsOneWidget);
  });

  testWidgets('Test the message card is tapped', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: const MessageList(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (_) => getScreenByName(settings.name!),
          settings: settings,
        );
      },
    ));
    expect(find.text("Account1"), findsOneWidget);

    final card = find.byKey(const Key('a'));
    await tester.tap(card); // Animate to the last page of on board
    await tester.pumpAndSettle();

    expect(find.text('Hasna Nadifah'), findsOneWidget);
  });
}
