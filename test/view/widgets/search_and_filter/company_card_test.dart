import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/view/screens/settings/settings_screen.dart';
import 'package:inme_mobile/view/widgets/search_and_filter/widgets.dart';

void main() {
  testWidgets("Test CompanyCard is pumped", (WidgetTester tester) async {
    final data = Business(
      id: 2,
      name: "Name",
      description: "Description",
      target: 100,
      category: "Food",
      investedFunds: 0,
    );
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: CompanyCard(companyData: data),
        ),
      ),
    );

    expect(find.text("Name"), findsWidgets);
  });
  testWidgets("Test CompanyCard with description > 80",
      (WidgetTester tester) async {
    final data = Business(
      id: 1,
      name: "Name",
      description:
          "aaaaaaaaaa aaaaaaaaaa aaaaaaaaaa aaaaaaaaaa aaaaaaaaaa aaaaaaaaaa aaaaaaaaaa aaaaaaaaaa",
      target: 100,
      category: "Food",
      investedFunds: 0,
    );
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: CompanyCard(companyData: data),
        ),
      ),
    );

    expect(find.text("Name"), findsWidgets);
  });
  testWidgets("Test CompanyCard on tapped", (WidgetTester tester) async {
    final data = Business(
      id: 1,
      name: "Name",
      description: "aaaaaaaaaa",
      target: 100,
      category: "Food",
      investedFunds: 0,
    );
    await tester.pumpWidget(
      MaterialApp(
        onGenerateRoute: (settings) {
          return MaterialPageRoute(builder: (context) => const Settings());
        },
        home: Scaffold(
          body: CompanyCard(companyData: data),
        ),
      ),
    );
    await tester.tap(find.byKey(const Key("company card")));
    await tester.pumpAndSettle();

    expect(find.text("Name"), findsNothing);
  });
}
