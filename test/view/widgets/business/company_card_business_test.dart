import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/view/widgets/business/company_card_business.dart';
import 'package:network_image_mock/network_image_mock.dart';

void main() {
  testWidgets('Test the company card business is created', (WidgetTester tester) async {
    await mockNetworkImagesFor(() => tester.pumpWidget(const MaterialApp(
          home: Scaffold(
              body: CompanyCardBusiness(
            companyData: [
              {
                'name': 'Nanaget',
                'nominal': '1.000.000',
                'logo': 'assets/images/nanaget.png',
              },
              {
                'name': "Fresh Food",
                'nominal': '10.000.000',
                'logo': 'assets/images/fresh_food.png',
              },
              {
                'name': 'Shoescare',
                'nominal': '1.000.000.000',
                'logo': 'assets/images/shoescare.png',
              }
            ],
            checker: true,
            index: 0,
          )),
        )));
    expect(find.text("Nanaget"), findsOneWidget);
  });
}
