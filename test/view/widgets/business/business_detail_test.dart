import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/view/screens/screens.dart';

void main() {
  testWidgets('Test the business detail screen is pumped', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (context) => const ViewBusiness(),
          settings: RouteSettings(
            arguments: Business(
              category: 'Kategori',
              description: 'Deskripsi',
              id: 10,
              investedFunds: 100,
              name: 'Nama',
              target: 1000,
            ),
          ),
        );
      },
    ));
    expect(find.text("Kategori"), findsOneWidget);
  });
}
