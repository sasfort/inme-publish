import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:image_picker/image_picker.dart';
import 'package:inme_mobile/utils/images.dart';
import 'package:inme_mobile/view/widgets/business/imagefield_form.dart';

void main() {
  testWidgets("Test ImageField is pumped", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: ImageField(
          key: _key,
          xFileImage: XFile(InMeImages.inMeLogo),
        ),
      ),
    ));

    expect(find.byKey(_key), findsWidgets);
  });
  testWidgets("Test ImageField and data is null", (WidgetTester tester) async {
    final _key = GlobalKey();
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: ImageField(
          key: _key,
          xFileImage: null,
        ),
      ),
    ));

    expect(find.byKey(_key), findsWidgets);
  });
}
