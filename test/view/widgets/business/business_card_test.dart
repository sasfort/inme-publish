import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/models/business.dart';
import 'package:inme_mobile/view/widgets/business/business_card.dart';
import 'package:network_image_mock/network_image_mock.dart';

void main() {
  testWidgets('Test the business card for My Business is created', (WidgetTester tester) async {
    Map<String, dynamic> data = {
      "id": 1,
      "business_name": "Ayam Goreng Buk Haji",
      "description": "Bisnis ayam goreng",
      "category": "Food",
      "target": "1000000",
      "invested_funds": "0"
    };
    Business business = Business.fromJson(data);

    await mockNetworkImagesFor(() => tester.pumpWidget(MaterialApp(
          home: Scaffold(body: BusinessCard(business: business)),
        )));
    expect(find.text("Ayam Goreng Buk Haji"), findsOneWidget);
  });
}
