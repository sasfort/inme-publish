import 'package:flutter_test/flutter_test.dart';
import 'package:inme_mobile/models/business.dart';

void main() {
  test("Test business model constructor", () {
    Business data = Business(
      id: 1,
      name: "Name 1",
      description: "Description 1",
      category: "Category 1",
      target: 100000.0,
      investedFunds: 10000.0,
    );

    expect(data.name, "Name 1");
  });

  test("Test business fromJson function", () {
    final data = {
      "id": 1,
      "business_name": "Name 1",
      "description": "Description 1",
      "category": "Category 1",
      "target": "100000",
      "invested_funds": "10000",
    };

    Business business = Business.fromJson(data);

    expect(business.name, "Name 1");
  });
}
