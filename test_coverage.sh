rm -f coverage/lcov.info
rm -f coverage/lcov_cleaned.info

flutter test --coverage

lcov -r coverage/lcov.info \*/view/screens/\* \*/providers/\* \*/sign_in/\* \*/utils/sign_in/\* \*/utils/api/\* \
     -o coverage/lcov_cleaned.info

genhtml coverage/lcov_cleaned.info --output=coverage